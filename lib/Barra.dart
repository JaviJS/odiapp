import 'package:flutter/material.dart';
import 'package:odiapp/modelo/Empleado.dart';
import 'package:odiapp/fallas.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:odiapp/notificaciones.dart';

class Barra extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => BarraState();
}

class BarraState extends State<Barra> {
  final db = Firestore.instance;
  String _user;
  Empleado empleado;
  NotificacionPage notificacionPage = new NotificacionPage("Notificaciones");

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("OdiApp"),
        backgroundColor: Color(0XFFBA68C8),
      ),
      drawer: new Drawer(
        child: new ListView(
          children: <Widget>[
            new UserAccountsDrawerHeader(
              accountEmail: FutureBuilder(
                  future: _cargarEmpleado(),
                  builder: (context, AsyncSnapshot<Empleado> snap) {
                    if (!snap.hasData) {
                      return Text("");
                    } else {
                      return Text(snap.data.nombre);
                    }
                  }),
              accountName: FutureBuilder(
                  future: _cargarEmpleado(),
                  builder: (context, AsyncSnapshot<Empleado> snap) {
                    if (!snap.hasData) {
                      return Text("");
                    } else {
                      return Text(snap.data.grupo);
                    }
                  }),
              currentAccountPicture: new GestureDetector(
                child: new CircleAvatar(
                  backgroundImage: AssetImage('assets/man.png'),
                ),
                onTap: () => print("Esta es tu cuenta"),
              ),
              decoration: new BoxDecoration(
                  image: new DecorationImage(
                      image: AssetImage('assets/luz.jpg'),
                      fit: BoxFit.fill)),
            ),
            new ListTile(
                title: new Text("Notificaciones"),
                trailing: new Icon(Icons.arrow_right),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => NotificacionPage("Notificaciones")));
                }),
            new ListTile(
                title: new Text("Fallas"),
                trailing: new Icon(Icons.arrow_right),
                onTap: () {
                  Navigator.of(context).pop();
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => FallasPage("Fallas")));
                }),
            new Divider(),
            new ListTile(
              title: new Text("Salir"),
              trailing: new Icon(Icons.exit_to_app),
              onTap: () => _logout(),
            ),
          ],
        ),
      ),
      body: Center(
        child: Text('Pagina de inicio'),
      ),
    );
  }
  _logout() async{
    await FirebaseAuth.instance.signOut();
    Navigator.of(context).pushReplacementNamed('/login');
  }

  Future<Empleado> _cargarEmpleado() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    setState(() {
      _user = user.uid;
    });
    DocumentSnapshot s = await Firestore.instance
        .collection('Empleado')
        .document(_user)
        .get()
        .then((snap) {
      setState(() {
        empleado = Empleado.fromMap(snap.data);
      });
    });
    return empleado;
  }
}
