import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:odiapp/modelo/Cliente.dart';
import 'package:odiapp/modelo/Falla.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'dart:io';

File image;
String filename;


class ModificarFalla extends StatefulWidget {
  final Falla falla;
  final Cliente cliente;
  final DocumentSnapshot snap;

  ModificarFalla({this.snap, this.falla, this.cliente});

  @override
  State<StatefulWidget> createState() => ModificarFallaState();
}

class ModificarFallaState extends State<ModificarFalla> {
  final db = Firestore.instance;
  final _formKey = GlobalKey<FormState>();

  DateTime _dateInicio;
  TimeOfDay _timeInicio;
  DateTime _dateTermino;
  TimeOfDay _timeTermino = new TimeOfDay.now();
  String categoriaId;
  String descripcion;
  String fallaImage;
  TextEditingController descripcionController;
  TextEditingController imageInputController;

  pickerCam() async {
    File img = await ImagePicker.pickImage(source: ImageSource.camera);
    if (img != null) {
      image = img;
      setState(() {});
    }
  }

  pickerGallery() async {
    File img = await ImagePicker.pickImage(source: ImageSource.gallery);
    if (img != null) {
      image = img;
      setState(() {});
    }
  }

  Widget divider() {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 80.0, horizontal: 4.0),
      child: Container(
        width: 0.8,
        color: Colors.black,
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    descripcionController = new TextEditingController(text: widget.falla.descripcion);
    categoriaId = widget.falla.categoria.documentID;
    _dateInicio=DateTime.parse(widget.falla.inicioFecha);
    _dateTermino=DateTime.parse(widget.falla.terminoFecha);
    _timeInicio=TimeOfDay(hour: int.parse(widget.falla.inicioHora.substring(0,2)), minute: int.parse(widget.falla.inicioHora.substring(3,5)));
    _timeTermino=TimeOfDay(hour: int.parse(widget.falla.terminoHora.substring(0,2)), minute: int.parse(widget.falla.terminoHora.substring(3,5)));

    fallaImage = widget.falla.image;

    print(widget.falla.image);
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _dateInicio,
        firstDate: new DateTime(2018),
        lastDate: new DateTime(2030));
    if (picked != null && picked != _dateInicio) {
      print('Date selected: ${_dateInicio.toString().substring(0, 10)}');
      setState(() {
        _dateInicio = picked;
      });
    }
  }

  Future<Null> _selectTime(BuildContext context) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: _timeInicio);
    if (picked != null && picked != _timeInicio) {
      print('Time selected: ${_timeInicio.toString().substring(10, 15)}');
      setState(() {
        _timeInicio = picked;
      });
    }
  }

  Future<Null> _selectDateTermino(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _dateTermino,
        firstDate: new DateTime(2018),
        lastDate: new DateTime(2030));
    if (picked != null && picked != _dateTermino) {
      print('Date selected: ${_dateTermino.toString().substring(0, 10)}');
      setState(() {
        _dateTermino = picked;
      });
    }
  }

  Future<Null> _selectTimeTermino(BuildContext context) async {
    final TimeOfDay picked =
        await showTimePicker(context: context, initialTime: _timeTermino);
    if (picked != null && picked != _timeTermino) {
      print('Time selected: ${_timeTermino.toString().substring(10, 15)}');
      setState(() {
        _timeTermino = picked;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Modificar falla'),
          backgroundColor: Color(0XFFBA68C8),
        ),
        body: Container(
          padding: EdgeInsets.all(20.0),
          child: Form(
            key: _formKey,
            child: ListView(
              children: <Widget>[
                Padding(
                  padding:
                  EdgeInsets.all(15.0),
                  child: Container(
                    child: Text('Modificar una Falla', style: TextStyle(
                      color: Colors.black54, fontWeight: FontWeight.bold,fontSize: 15.0,
                    ),),
                  ),
                ),
                Padding(
                  padding:
                  EdgeInsets.all(5.0),
                  child: Container(
                      child: Text(widget.cliente.nombre, style: TextStyle(fontWeight: FontWeight.bold,color: Colors.black54,),)),
                ),
                Row(
                  children: <Widget>[
                    Icon(
                      Icons.location_on,
                      color: Colors.black54,
                      size: 15.0,
                    ),
                    Padding(
                      padding:
                      EdgeInsets.all(5.0),
                      child: Container(
                          child: Text(widget.cliente.nombreDireccion)),
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Icon(Icons.date_range,
                      color: Colors.black54,
                      size: 15.0,
                    ),
                    Padding(
                      padding:
                      EdgeInsets.all(5.0),
                      child: Container(
                        child: Text(
                          widget.falla.llamadaFecha.toString(), style: TextStyle(
                          color: Colors.black54,
                        ),),
                      ),
                    ),
                    Icon(Icons.watch_later,
                      color: Colors.black54,
                      size: 15.0,
                    ),
                    Padding(
                      padding:
                      EdgeInsets.all(5.0),
                      child: Container(
                        child: Text(
                          widget.falla.llamadaHora.toString(), style: TextStyle(
                            color: Colors.black54
                        ),),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding:
                  EdgeInsets.all(15.0),
                  child: Container(
                    child: Text('Datos de inicio de trabajo', style: TextStyle(
                      color: Colors.black54, fontWeight: FontWeight.bold,fontStyle:FontStyle.italic,fontSize: 15.0,
                    ),),
                  ),
                ),

                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.date_range,
                        color: Colors.black54,
                      ),
                      onPressed: () => _selectDate(context),
                    ),
                    Container(
                      child: Flexible(
                        child: TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              labelText:
                              _dateInicio.toString().substring(0, 10)),
                        ),
                      ),
                    )
                  ],
                ),

                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.watch_later,
                        color: Colors.black54,
                      ),
                      onPressed: () => _selectTime(context),
                    ),
                    Container(
                      child: Flexible(
                        child: TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              labelText: _timeInicio.toString().substring(10, 15)),
                        ),
                      ),
                    )
                  ],
                ),

                Padding(
                  padding:
                  EdgeInsets.all(15.0),
                  child: Container(
                    child: Text('Datos de termino de trabajo', style: TextStyle(
                      color: Colors.black54, fontWeight: FontWeight.bold,fontStyle:FontStyle.italic,fontSize: 15.0,
                    ),),
                  ),
                ),

                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.date_range,
                        color: Colors.black54,
                      ),
                      onPressed: () => _selectDateTermino(context),
                    ),
                    Container(
                      child: Flexible(
                        child:
                        TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              labelText: _dateTermino.toString().substring(0, 10)),
                        ),
                      ),
                    )
                  ],
                ),

                Row(
                  children: <Widget>[
                    IconButton(
                      icon: Icon(
                        Icons.watch_later,
                        color: Colors.black54,
                      ),
                      onPressed: () => _selectTimeTermino(context),
                    ),
                    Container(
                      child: Flexible(
                        child:
                        TextFormField(
                          enabled: false,
                          decoration: InputDecoration(
                              labelText: _timeTermino.toString().substring(10, 15)),
                        ),
                      ),
                    )
                  ],
                ),

                Padding(
                  padding:
                  EdgeInsets.all(15.0),
                  child: Container(
                    child: Text('Dascripción de falla', style: TextStyle(
                      color: Colors.black54, fontWeight: FontWeight.bold,fontStyle:FontStyle.italic,fontSize: 15.0,
                    ),),
                  ),
                ),
                Padding(
                  padding:
                  EdgeInsets.all(15.0),
                  child: Container(
                    child: TextField(
                      maxLines: null,
                      controller: descripcionController,
                      decoration: InputDecoration(
                          labelText: 'Escribe una descripción',
                          icon: const Icon(
                            Icons.chrome_reader_mode,
                            color: Colors.black54,
                            size: 15.0,
                          )),
                      keyboardType: TextInputType.multiline,
                      onChanged: (val) {
                        setState(() {
                          descripcion = val;
                        });
                      },
                    ),
                  ),
                ),
                Padding(
                  padding:
                  EdgeInsets.all(15.0),
                  child: Container(
                    child: Text('Categoria de falla', style: TextStyle(
                      color: Colors.black54, fontWeight: FontWeight.bold,fontStyle:FontStyle.italic,fontSize: 15.0,
                    ),),
                  ),
                ),

                Padding(
                  padding:
                  EdgeInsets.all(15.0),
                  child: Container(
                    child: FormField(
                      builder: (FormFieldState state) {
                        return InputDecorator(
                          decoration: InputDecoration(
                            icon: const Icon(
                              Icons.build,
                              color: Colors.black54,
                              size: 15.0,
                            ),
                          ),
                          child: _categorias(),
                        );
                      },
                    ),
                  ),
                ),

                Row(
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Container(
                              height: 100.0,
                              width: 100.0,
                              decoration: new BoxDecoration(
                                  border: new Border.all(color: Colors.black54),
                                  borderRadius: BorderRadius.circular(5.0)
                              ),
                              padding: new EdgeInsets.all(5.0),
                              child: image == null ? Text('Add',style: TextStyle(color: Colors.black54),) : Image.file(image),
                            ),
                            Container(
                              padding: const EdgeInsets.only(left: 2.2),
                              child: new Container(
                                height: 100.0,
                                width: 100.0,
                                decoration: new BoxDecoration(
                                    border: new Border.all(color: Colors.black54),
                                    borderRadius: BorderRadius.circular(5.0)
                                ),
                                padding: new EdgeInsets.all(5.0),
                                child: fallaImage == '' ? Text('Edit'): Image.network(fallaImage+'?alt=media'),
                              ),
                            ),
                          ],
                        ),

                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Divider(),
                        /*
                        new IconButton(
                            icon: new Icon(Icons.camera_alt), onPressed: pickerCam),*/
                        Divider(),
                        new IconButton(
                            icon: new Icon(Icons.image), onPressed: pickerGallery),
                      ],
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: RaisedButton(
                      onPressed: () {
                        DateTime now = DateTime.now();
                        String nuevoFormato = DateFormat('kk:mm:ss:MMMMd').format(now);
                        var fullImageName = 'nomfoto-$nuevoFormato' + '.jpg';
                        var fullImageName2 = 'nomfoto-$nuevoFormato' + '.jpg';

                        final StorageReference ref =
                        FirebaseStorage.instance.ref().child(fullImageName);
                        final StorageUploadTask task = ref.putFile(image);

                        var part1 =
                            'https://firebasestorage.googleapis.com/v0/b/odiapp.appspot.com/o/';
                        var fullPathImage = part1 + fullImageName2;

                        Firestore.instance
                            .collection('falla')
                            .document(widget.snap.documentID)
                            .updateData({
                          'estado': true,
                          'inicio': DateTime.parse(
                              '${_dateInicio.toString().substring(0, 10)} ${_timeInicio.toString().substring(10, 15)}:00.0000'),
                          'termino': DateTime.parse(
                              '${_dateTermino.toString().substring(0, 10)} ${_timeTermino.toString().substring(10, 15)}:00.0000'),
                          'categoria': Firestore.instance
                              .collection('categoria')
                              .document(categoriaId),
                          'descripcion': descripcionController.text,
                          'image': fullPathImage,
                        });
                        Navigator.of(context)
                            .pop(); //regrese a la pantalla anterior
                      },
                      color: Color(0XFFBA68C8),
                      child: Text('Guardar',
                          style: TextStyle(fontSize: 20, color: Colors.white)),
                      shape: new RoundedRectangleBorder(
                          borderRadius: new BorderRadius.circular(10.0))),
                ),
              ],
            ),
          ),
        ));
  }

  _onCategoryDropItemSelected(String newValueSelected) {
    setState(() {
      this.categoriaId = newValueSelected;
    });
  }

  Widget _categorias() {
    return StreamBuilder<QuerySnapshot>(
        stream: Firestore.instance.collection('categoria').snapshots(),
        builder: (context, snapshot) {
          if (!snapshot.hasData) {
            return Center(
              child: CupertinoActivityIndicator(),
            );
          }
          return Container(
            child: DropdownButton(
              value: categoriaId,
              onChanged: (valueSelectedByUser) {
                _onCategoryDropItemSelected(valueSelectedByUser);
              },
              items: snapshot.data.documents.map((DocumentSnapshot document) {
                return DropdownMenuItem<String>(
                  value: document.documentID,
                  child: Container(
                    width: 190.0,
                    child: Text(document.data['nombre']),
                  ),
                );
              }).toList(),
            ),
          );
        });
  }

}
