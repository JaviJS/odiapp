import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';


class LoginPage extends StatefulWidget{
  @override
  State<StatefulWidget> createState()=> LoginPageState();
}
 class LoginPageState extends State<LoginPage>{

  final _formKey = GlobalKey<FormState>();

  String _mail;
  String _password;


  Future<void> _login() async{
    final form = _formKey.currentState;
    if(form.validate()){
      form.save();
      try{
        await FirebaseAuth.instance.signInWithEmailAndPassword(
            email: _mail, password: _password);
        Navigator.of(context).pushReplacementNamed('/barra');
      }catch(e){
        _neverSatisfied();
      }
    }
  }

  Future<void> _neverSatisfied() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text('Usuario no autentificado'),
          shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0))),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Correo electronico y/o contraseña sin coincidencias. Intentelo nuevamente')
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text('Cerrar'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  @override
   Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
        backgroundColor: Color(0XFFBA68C8),
      ),
      body: Container(
        padding: EdgeInsets.all(20.0),
        child: Form(
          key:_formKey,
          child: ListView(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top:20,bottom: 20.0),
                child:Image.asset('assets/logo.png'),
              ),
              TextFormField(
                autocorrect: false,
                keyboardType: TextInputType.emailAddress,
                decoration:InputDecoration(labelText: 'Correo electronico'),
                validator: (val){
                  if(val.isEmpty){
                    return 'Please enter a valid email';
                  }else{
                    return null;
                  }
                },
                onSaved: (val){
                  setState((){
                    _mail=val;
                  });
                },
              ),
              TextFormField(
                obscureText: true,
                decoration:InputDecoration(labelText: 'Contraseña'),
                validator: (val){
                  if(val.isEmpty){
                    return 'Please enter a valid password';
                  }else{
                    return null;
                  }
                },
                onSaved: (val){
                  setState((){
                    _password=val;
                  });
                },
              ),
            Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child:RaisedButton(
                  onPressed: (){_login();},
                  color: Color(0XFFBA68C8),
                  child: Text('Entrar', style: TextStyle(fontSize: 20,color: Colors.white)),
                  shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0))
              ),
            )
            ],
          ),
        ),
      )
    );
  }
 }