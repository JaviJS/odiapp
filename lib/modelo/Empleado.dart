import 'package:cloud_firestore/cloud_firestore.dart';

class Empleado{
  String id;
  String nombre;
  String telefono;
  String grupo;

  Empleado(this.grupo,this.nombre,this.telefono);

  Empleado.fromMap(Map<String, dynamic> map)
      : assert(map['nombre'] != null),
        assert(map['telefono'] != null),
        assert(map['grupo'] != null),
        nombre = map['nombre'],
        telefono = map['nombreDireccion'],
        grupo=map['grupo'];

}