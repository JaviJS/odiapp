import 'package:cloud_firestore/cloud_firestore.dart';

class Cliente{
  String nombre;
  String nombreDireccion;
  GeoPoint direccion;

  Cliente.fromMap(Map<String, dynamic> map)
      : assert(map['nombre'] != null),
        assert(map['nombreDireccion'] != null),
        nombre = map['nombre'],
        nombreDireccion = map['nombreDireccion'],
        direccion = map['direccion'];

  Cliente.fromSnapshot(DocumentSnapshot snapshot)
      : this.fromMap(snapshot.data);

}