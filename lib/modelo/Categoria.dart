
import 'package:cloud_firestore/cloud_firestore.dart';
class Categoria{
  String id;
  String nombre="";
  Categoria(this.nombre);

  Categoria.fromMap(Map<String, dynamic> map)
      : assert(map['nombre'] != null),
        nombre = map['nombre'];

  Categoria.fromSnapshot(DocumentSnapshot snapshot)
      : nombre = snapshot['nombre'];
}

