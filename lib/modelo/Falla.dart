import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:intl/intl.dart';

class Falla {
  final bool estado;
  final String llamadaFecha;
  final String llamadaHora;
  final String grupo;
  final DocumentReference cliente;

  //no obligatorios
  DocumentReference categoria;
  String descripcion;
  String inicioFecha;
  String inicioHora;
  String terminoFecha;
  String terminoHora;
  String image;

  Falla.fromMap(Map<String, dynamic> map)
      : assert(map['estado'] != null),
        assert(map['cliente'] != null),
        assert(map['llamada'] != null),
        assert(map['grupo'] != null),
        estado = map['estado'],
        llamadaFecha =
            new DateFormat('yyyy-MM-dd').format(map['llamada'].toDate()),
        llamadaHora =
            new DateFormat('HH:mm:ss').format(map['llamada'].toDate()),
        cliente = map['cliente'],
        grupo = map['grupo'];

  Falla.fromMapFall(Map<String, dynamic> map)
      : assert(map['estado'] != null),
        assert(map['cliente'] != null),
        assert(map['llamada'] != null),
        assert(map['grupo'] != null),
        estado = map['estado'],
        llamadaFecha =
            new DateFormat('yyyy-MM-dd').format(map['llamada'].toDate()),
        llamadaHora =
            new DateFormat('HH:mm:ss').format(map['llamada'].toDate()),
        cliente = map['cliente'],
        grupo = map['grupo'],
        categoria = map['categoria'],
        image = map['image'],
        descripcion = map['descripcion'],
        inicioFecha =
            new DateFormat('yyyy-MM-dd').format(map['inicio'].toDate()),
        inicioHora = new DateFormat('HH:mm:ss').format(map['inicio'].toDate()),
        terminoHora =
            new DateFormat('HH:mm:ss').format(map['termino'].toDate()),
        terminoFecha =
            new DateFormat('yyyy-MM-dd').format(map['termino'].toDate());

  Falla.fromSnapshot(DocumentSnapshot snapshot) : this.fromMap(snapshot.data);
}
