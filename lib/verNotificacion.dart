import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:odiapp/modelo/Cliente.dart';
import 'package:odiapp/modelo/Falla.dart';
import 'package:flutter/cupertino.dart';
import 'package:odiapp/responderNotificacion.dart';
import 'dart:async';
import 'package:location/location.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class VerNotificacion extends StatefulWidget {
  final Falla sn;
  final Cliente cl;
  final DocumentSnapshot s;

  VerNotificacion({this.s, this.sn, this.cl});

  @override
  State<StatefulWidget> createState() => VerNotificacionState();
}

class VerNotificacionState extends State<VerNotificacion> {
  final db = Firestore.instance;
  final _formKey = GlobalKey<FormState>();
  final Set<Marker> _markers = {};

  Map<String, double> currentLocation = new Map();
  StreamSubscription<Map<String, double>> locationSubscription;

  Location _location = new Location();

  bool isLoading = false;
  String error;
  Completer<GoogleMapController> _controller = Completer();

  @override
  void initState() {
    super.initState();
    currentLocation['latitude'] = 0.0;
    currentLocation['longitude'] = 0.0;
    loadLocalizacion();
    locationSubscription =
        _location.onLocationChanged().listen((Map<String, double> result) {
      currentLocation = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Notificación'),
        backgroundColor: Color(0XFFBA68C8),
      ),
      body: Stack(
        children: <Widget>[
          _googleMap(context),
          _buildContainer(),
        ],
      ),
    );
  }

  Widget _googleMap(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: GoogleMap(
        mapType: MapType.normal,
        initialCameraPosition: CameraPosition(
            target: LatLng(
                widget.cl.direccion.latitude, widget.cl.direccion.longitude),
            zoom: 12.0),
        onMapCreated: (GoogleMapController controller) {
          _controller.complete(controller);
        },
        markers: _markers,
      ),
    );
  }

  Widget _buildContainer() {
    return Align(
      alignment: Alignment.bottomLeft,
      child: Container(
          margin: EdgeInsets.symmetric(vertical: 20.0),
          height: 150.0,
          child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: ListView(
                scrollDirection: Axis.horizontal,
                children: <Widget>[
                  FittedBox(
                    child: Material(
                      color: Colors.white,
                      elevation: 14.0,
                      borderRadius: BorderRadius.circular(24.0),
                      shadowColor: Color(0x002196F3),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: _boxes(),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ))),
    );
  }

  Widget _boxes() {
    return GestureDetector(
      child: Container(
        child: new FittedBox(
          child: Material(
              color: Colors.white,
              borderRadius: BorderRadius.circular(24.0),
              shadowColor: Color(0x802196F3),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: myDetailsContainer1(),
                    ),
                  ),
                ],
              )),
        ),
      ),
    );
  }

  Widget myDetailsContainer1() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 8.0),
          child: Container(
              child: Text(
            widget.cl.nombre,
            style: TextStyle(
                color: Color(0xff6200ee),
                fontSize: 24.0,
                fontWeight: FontWeight.bold),
          )),
        ),
        SizedBox(height: 5.0),
        Container(
            child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
                child: Text(
              widget.cl.nombreDireccion,
              style: TextStyle(
                color: Colors.black54,
                fontSize: 18.0,
              ),
            )),
          ],
        )),
        SizedBox(height: 5.0),
        Container(
            child: Text(
          widget.sn.llamadaFecha + ' ' + widget.sn.llamadaHora,
          style: TextStyle(color: Colors.black54, fontSize: 18.0),
        )),
        SizedBox(height: 5.0),
        Container(
            child: Row(
          children: <Widget>[
            Icon(Icons.location_on, color: Color(0xff6200ee)),
            Text(
              '[' +
                  widget.cl.direccion.latitude.toString() +
                  ',' +
                  widget.cl.direccion.longitude.toString() +
                  ']',
              style: TextStyle(
                  color: Colors.black54,
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold),
            )
          ],
        )),
        SizedBox(height: 5.0),
        RaisedButton(
          onPressed: () => _notificacion(widget.s, widget.sn, widget.cl),
          shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(30.0)),
          color: Color(0xFFD1C4E9),
          child: Text('Responder',
              style:
                  TextStyle(fontWeight: FontWeight.bold, color: Colors.white)),
        ),
      ],
    );
  }

  Future<void> _notificacion(
      DocumentSnapshot snap, Falla falla, Cliente cliente) async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ResponderNotificacion(
                  falla: falla,
                  cliente: cliente,
                  snap: snap,
                )));
  }

  Future<void> _volverAtras() async {
    Navigator.of(context).pushReplacementNamed('/prueba');
  }

  void loadLocalizacion() async {
    Map<String, double> location;
    try {
      location = await _location.getLocation();

      error = null;
    } on PlatformException catch (e) {
      if (e.code == 'PERMISSION_DENIED') {
        error = 'Permission denied';
      } else if (e.code == 'PERMISSION_DENIED_NEVER_ASK') {
        error =
            'Permission denied - please ask the user to enable it from the app settings';
      }

      location = null;
    }

    setState(() {
      currentLocation = location;
      _markers.add(Marker(
        markerId: MarkerId('miPosicion'),
        position:
            LatLng(currentLocation['latitude'], currentLocation['longitude']),
        infoWindow: InfoWindow(title: 'Mi Posición'),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
      ));
      _markers.add(Marker(
        markerId: MarkerId('posicionCliente'),
        position:
            LatLng(widget.cl.direccion.latitude, widget.cl.direccion.longitude),
        infoWindow: InfoWindow(title: 'Posicion Destino'),
        icon: BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueViolet),
      ));
    });
  }
}
