import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:odiapp/modelo/Empleado.dart';
import 'package:odiapp/modelo/Cliente.dart';
import 'package:odiapp/modelo/Falla.dart';
import 'verNotificacion.dart';

class NotificacionPage extends StatefulWidget {
  final String title;

  NotificacionPage(this.title);
  @override
  State<StatefulWidget> createState() => NotificacionPageState();
}


class NotificacionPageState extends State<NotificacionPage> {
  final db = Firestore.instance;
  String _user;
  Empleado as;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(title: new Text(widget.title), backgroundColor: Color(0XFFBA68C8),),
        body: Container(
          decoration: new BoxDecoration(color: Colors.white70),
          child: FutureBuilder(
              future: _cargarEmpleado(),
              builder: (context, AsyncSnapshot<Empleado> snap) {
                if (!snap.hasData) {
                  return CircularProgressIndicator();
                } else {
                  return _cargarFallas(context);
                }
              }),
        ));
  }

  Widget _cargarFallas(BuildContext context) {
    return StreamBuilder(
        stream: Firestore.instance
            .collection('falla')
            .where("estado", isEqualTo: false)
            .where("grupo", isEqualTo: as.grupo)
            .snapshots(),
        builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (!snapshot.hasData) {
            return CircularProgressIndicator();
          }
          int length = snapshot.data.documents.length;
          return ListView.builder(
              itemCount: length,
              padding: EdgeInsets.all(6.0),
              itemBuilder: (_, int index) {
                final DocumentSnapshot doc = snapshot.data.documents[index];
                final Falla doc2 = Falla.fromSnapshot(doc);
                return new Container(
                    margin: const EdgeInsets.only(
                        top: 6.0, bottom: 6.0, left: 12.0, right: 12.0),
                    child: Card(
                      child: Container(
                        padding: EdgeInsets.all(10.0),
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(5.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black54,
                              blurRadius: 20.0,
                              // has the effect of softening the shadow
                              spreadRadius: 1.0,
                              // has the effect of extending the shadow
                              offset: Offset(
                                5.0, // horizontal, move right 10
                                5.0, // vertical, move down 10
                              ),
                            )
                          ],
                        ),
                        child: Row(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(left: 10.0),
                              height: 40.0,
                              width: 40.0,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                      bottomLeft: Radius.circular(5),
                                      topLeft: Radius.circular(5)),
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage('assets/siren.png'))),
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 20.0, top: 12.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  FutureBuilder(
                                      future: _cargarCliente(
                                          doc2.cliente.documentID),
                                      builder: (context,
                                          AsyncSnapshot<Cliente> snap) {
                                        if (!snap.hasData) {
                                          return Text("");
                                        } else {
                                          return InkWell(
                                            child: Container(
                                              child: Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  Padding(
                                                    padding:
                                                        EdgeInsets.all(5.0),
                                                    child: Container(
                                                        child: Text(
                                                      snap.data.nombre,
                                                      style: TextStyle(
                                                          fontWeight:
                                                              FontWeight.bold,
                                                          color:
                                                              Colors.black54),
                                                    )),
                                                  ),
                                                  Row(
                                                    children: <Widget>[
                                                      Icon(
                                                        Icons.location_on,
                                                        color: Colors.black54,
                                                        size: 15.0,
                                                      ),
                                                      Padding(
                                                        padding:
                                                        EdgeInsets.all(5.0),
                                                        child: Container(
                                                            child: Text(snap
                                                                .data
                                                                .nombreDireccion)),
                                                      ),
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                            onTap: () =>
                                                _verNot(doc, doc2, snap.data),
                                          );
                                        }
                                      }),
                                  Row(
                                    children: <Widget>[
                                      Icon(Icons.date_range,
                                        color: Colors.black54,
                                        size: 15.0,
                                      ),
                                      Padding(
                                        padding:
                                        EdgeInsets.all(5.0),
                                        child: Container(
                                          child: Text(
                                            doc2.llamadaFecha.toString(), style: TextStyle(
                                              color: Colors.black54
                                          ),),
                                        ),
                                      ),
                                      Icon(Icons.watch_later,
                                        color: Colors.black54,
                                        size: 15.0,
                                      ),
                                      Padding(
                                        padding:
                                        EdgeInsets.all(5.0),
                                        child: Container(
                                          child: Text(
                                            doc2.llamadaHora.toString(), style: TextStyle(
                                              color: Colors.black54
                                          ),),
                                        ),
                                      ),
                                    ],
                                  ),

                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    ));
              });
        });
  }

  //metodo para ir a ventana anterior
  Future<void> _volverAtras() async {
    Navigator.of(context).pushReplacementNamed('/prueba');
  }

  Future<void> _verNot(
      DocumentSnapshot snap, Falla falla, Cliente cliente) async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => VerNotificacion(
                  sn: falla,
                  cl: cliente,
                  s: snap,
                )));
  }

  //metodo para obtener los datos del cliente de la falla
  Future<Cliente> _cargarCliente(id) async {
    var _as;
    DocumentSnapshot s = await Firestore.instance
        .collection('Cliente')
        .document(id)
        .get()
        .then((snap) {
      setState(() {
        _as = Cliente.fromMap(snap.data);
      });
    });
    return _as;
  }

  //metodo para cargar datos de empleado el cual tiene sesion
  Future<Empleado> _cargarEmpleado() async {
    FirebaseUser user = await FirebaseAuth.instance.currentUser();
    setState(() {
      _user = user.uid;
    });
    DocumentSnapshot s = await Firestore.instance
        .collection('Empleado')
        .document(_user)
        .get()
        .then((snap) {
      setState(() {
        as = Empleado.fromMap(snap.data);
      });
    });
    return as;
  }
}
