import 'package:flutter/material.dart';
import 'package:odiapp/login.dart';
import 'package:odiapp/routes.dart';
import 'package:odiapp/Barra.dart';
import 'package:firebase_auth/firebase_auth.dart';
void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
    @override
    State<StatefulWidget> createState()=> MyAppState();
  }

  class MyAppState extends State<MyApp>{
  Widget rootPage= Barra();

  Future<Widget> getRootPage() async =>
  await FirebaseAuth.instance.currentUser() == null
    ? LoginPage()
    : Barra();

  @override
  void initState() {
    super.initState();
    getRootPage().then((Widget page) {
      setState(() {
        rootPage = page;
      });
    });
  }

  @override
    Widget build(BuildContext context) {
    // TODO: implement build
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      home: rootPage,
      routes: buildAppRoutes(),
    );
  }
}

