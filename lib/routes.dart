import 'package:flutter/material.dart';
import 'package:odiapp/login.dart';
import 'package:odiapp/Barra.dart';


Map<String, WidgetBuilder> buildAppRoutes(){
  return{
    '/login': (BuildContext context) => new LoginPage(),
    '/barra':(BuildContext context) => new Barra(),
  };
}