import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:odiapp/modelo/Cliente.dart';
import 'package:odiapp/modelo/Falla.dart';
import 'package:flutter/cupertino.dart';
import 'package:odiapp/modificarFalla.dart';
import 'package:odiapp/modelo/Categoria.dart';

class VerFalla extends StatefulWidget {
  final Falla sn;
  final Cliente cl;
  final DocumentSnapshot s;

  VerFalla({this.s, this.sn, this.cl});

  @override
  State<StatefulWidget> createState() => VerFallaState();
}

class VerFallaState extends State<VerFalla> {
  final db = Firestore.instance;
  final _formKey = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Falla'),
          backgroundColor: Color(0XFFBA68C8),
        ),
        body: Card(
          margin: EdgeInsets.all(10.0),
          child: InkWell(
            splashColor: Colors.blue.withAlpha(30),
            child: Container(
              child: ListView(
                children: <Widget>[
                  Container(
                    height: 150.0,
                    width: 90.0,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.all(Radius.circular(5.0)),
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image:
                                NetworkImage(widget.sn.image + '?alt=media'))),
                  ),
                  Container(
                      margin: EdgeInsets.all(10.0),
                      child: Row(
                        children: <Widget>[
                          Text(
                            widget.cl.nombre,
                            style: TextStyle(
                                color: Colors.black54,
                                fontSize: 18.0,
                                fontWeight: FontWeight.bold),
                          )
                        ],
                      )),
                  Container(
                      child: Row(
                    children: <Widget>[
                      Icon(Icons.location_on, color: Colors.black54),
                      Text(
                        widget.cl.nombreDireccion,
                        style: TextStyle(
                            color: Colors.black54,
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold),
                      )
                    ],
                  )),
                  Padding(
                    padding:
                    EdgeInsets.all(15.0),
                    child: Container(
                      child: Text('Datos de solicitud de falla', style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold,fontStyle:FontStyle.italic,fontSize: 15.0,
                      ),),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.date_range,
                        color: Colors.black54,
                        size: 15.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Container(
                          child: Text(
                            widget.sn.llamadaFecha,
                            style: TextStyle(color: Colors.black54),
                          ),
                        ),
                      ),
                      Icon(
                        Icons.watch_later,
                        color: Colors.black54,
                        size: 15.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Container(
                          child: Text(
                            widget.sn.llamadaHora.toString(),
                            style: TextStyle(color: Colors.black54),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                    EdgeInsets.all(15.0),
                    child: Container(
                      child: Text('Datos de inicio de trabajo', style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold,fontStyle:FontStyle.italic,fontSize: 15.0,
                      ),),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.date_range,
                        color: Colors.black54,
                        size: 15.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Container(
                          child: Text(
                            widget.sn.inicioFecha,
                            style: TextStyle(color: Colors.black54),
                          ),
                        ),
                      ),
                      Icon(
                        Icons.watch_later,
                        color: Colors.black54,
                        size: 15.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Container(
                          child: Text(
                            widget.sn.inicioHora.toString(),
                            style: TextStyle(color: Colors.black54),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                    EdgeInsets.all(15.0),
                    child: Container(
                      child: Text('Datos de termino de trabajo', style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold,fontStyle:FontStyle.italic,fontSize: 15.0,
                      ),),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.date_range,
                        color: Colors.black54,
                        size: 15.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Container(
                          child: Text(
                            widget.sn.terminoFecha,
                            style: TextStyle(color: Colors.black54),
                          ),
                        ),
                      ),
                      Icon(
                        Icons.watch_later,
                        color: Colors.black54,
                        size: 15.0,
                      ),
                      Padding(
                        padding: EdgeInsets.all(5.0),
                        child: Container(
                          child: Text(
                            widget.sn.terminoHora.toString(),
                            style: TextStyle(color: Colors.black54),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                    EdgeInsets.all(15.0),
                    child: Container(
                      child: Text('Categoria de la falla:', style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold,fontStyle:FontStyle.italic,fontSize: 15.0,
                      ),),
                    ),
                  ),
                  FutureBuilder(
                      future: _cargarCategoria(widget.sn.categoria.documentID),
                      builder: (context, AsyncSnapshot<Categoria> snap) {
                        if (!snap.hasData) {
                          return Text("");
                        } else {
                          return Container(
                            child: Padding(
                              padding: EdgeInsets.fromLTRB(0, 3, 0, 3),
                              child: Container(
                                  width: 200.0, child: Text(snap.data.nombre)),
                            ),
                          );
                        }
                      }),

                  Padding(
                    padding:
                    EdgeInsets.all(15.0),
                    child: Container(
                      child: Text('Descripcion de la falla', style: TextStyle(
                        color: Colors.black54, fontWeight: FontWeight.bold,fontStyle:FontStyle.italic,fontSize: 15.0,
                      ),),
                    ),
                  ),
                  Text(widget.sn.descripcion.toString()),
                  SizedBox(height: 5.0),

                  Container(
                    alignment: Alignment.bottomCenter,
                    child: SizedBox(
                      width: 130.0,
                      child: RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(30.0)),
                          color: Color(0XFFBA68C8),
                          onPressed: () =>
                              _editarFalla(widget.s, widget.sn, widget.cl),
                          child: Row(
                            children: <Widget>[
                              Icon(
                                Icons.edit,
                                color: Colors.white70,
                                size: 15.0,
                              ),
                              Text("Editar Falla",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white)),
                            ],
                          )),
                    )
                  ),
                ],
              ),
            ),
          ),
        ));
  }

  Future<void> _editarFalla(
      DocumentSnapshot snap, Falla falla, Cliente cliente) async {
    Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ModificarFalla(
                  falla: falla,
                  cliente: cliente,
                  snap: snap,
                )));
  }

  Future<void> _volverAtras() async {
    Navigator.of(context).pushReplacementNamed('/prueba');
  }

  Future<Categoria> _cargarCategoria(id) async {
    var _categoria;
    DocumentSnapshot s = await Firestore.instance
        .collection('categoria')
        .document(id)
        .get()
        .then((snap) {
      setState(() {
        _categoria = Categoria.fromMap(snap.data);
      });
    });
    return _categoria;
  }
}
